﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPL_Project_Enterprise {
    public abstract class Entity {
        public abstract string Id { get; set; }
    }

public abstract class Repository<T> where T : Entity, new() {
        protected List<T> list;
        protected DataVerbs Verb;
        protected string UserId = "pac_longmaser";
        protected string PassKey = "@jala12345!";

        public string DetaProcess {
            get {
                Verb = DataVerbs.Get;
                var list = DataRetriving(Detail, (Model == null) ? Query(new T()) : Query(Model));
                return JsonConvert.SerializeObject(list);
            }
}
